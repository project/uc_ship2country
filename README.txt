Description
-----------
        
A very basic module for Ubercart that allows to specify shipping countries on a product level.


Author
------

Written by: Sjoerd Arendsen (http://www.optixdesigns.com)

If you use this module, find it useful, and want to send the author
a thank you note, then use the Feedback/Contact page at the URL above.
